class Topic < ActiveRecord::Migration
  def up
    create_table :topics do |t|
      t.string :title
      t.string :sub_title
      t.references :author, index: true
      t.references :section, index: true
      t.timestamps
    end
  end

  def down
    drop table :topics
  end
end
