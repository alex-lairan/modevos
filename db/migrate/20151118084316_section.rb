class Section < ActiveRecord::Migration
  def up
    create_table :sections do |t|
      t.string :title

      t.timestamps
    end
  end

  def down
    drop table :sections
  end
end
