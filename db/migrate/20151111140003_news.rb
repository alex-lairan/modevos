class News < ActiveRecord::Migration
  def up
    create_table :news do |t|
      t.string :title
      t.string :image
      t.text :description
      t.text :content

      t.timestamps
    end
  end

  def down
    drop table :news
  end
end
