class Message < ActiveRecord::Migration
  def up
    create_table :messages do |t|
      t.text :content
      t.references :author, index: true
      t.references :topic, index: true
      
      t.timestamps
    end
  end

  def down
    drop table :messages
  end
end
