# News
class NewsController < ApplicationController
  before_action :set_new, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:edit, :update, :destroy,
                                            :new, :create]
  # GET /news
  # GET /news.json
  def index
    @news = New.all.reverse_order
    respond_to do |format|
      format.html
      format.json { render json: @news }
    end
  end

  # GET /news/1
  # GET /news/1.json
  def show
    respond_to do |format|
      format.html
      format.json { render json: @new }
    end
  end

  # GET /news/new
  def new
    @new = New.new
  end

  # GET /news/1/edit
  def edit
  end

  # POST /news
  # POST /news.json
  def create
    @new = New.new(new_params)

    respond_to do |format|
      if @new.save
        format.html do
          redirect_to @new, notice: 'News was successfully created.'
        end
        format.json do
          render :show, status: :created, location: @new
        end
      else
        format.html do
          render :new
        end
        format.json do
          render json: @new.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /news/1
  # PATCH/PUT /news/1.json
  def update
    respond_to do |format|
      if @new.update(new_params)
        format.html do
          redirect_to @new, notice: 'L\'actualite à correctement été modifié.'
        end
        format.json do
          render :show, status: :ok, location: @new
        end
      else
        format.html do
          redirect_to edit_news_path(@new),
                      alert: 'Erreur lors de l\'édition de l\'actualite'
        end
        format.json do
          render json: @new.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /news/1
  # DELETE /news/1.json
  def destroy
    @new.destroy
    respond_to do |format|
      format.html do
        redirect_to news_index_path,
                    notice: 'News was successfully destroyed.'
      end
      format.json do
        head :no_content
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_new
    @new = New.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list
  def new_params
    params.require(:new).permit(:title, :image, :description, :content)
  end
end
