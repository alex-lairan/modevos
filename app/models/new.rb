#
class New < ActiveRecord::Base
  validates :title,       presence: true
  validates :image,       presence: true
  validates :description, presence: true
  validates :content,     presence: true

  mount_uploader :image, NewsUploader
end
